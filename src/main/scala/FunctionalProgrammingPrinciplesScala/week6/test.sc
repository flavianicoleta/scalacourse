object test {
  val xs = Array(1, 2 ,3, 44)

  xs map (x => x*2)

  val s = "Hello world"
  s.filter(_.isUpper)

  1 to 5
  1 until 5
  1 to 3 by 3
  6 to 1 by -2

  s.exists(c => c.isUpper)
  s.forall(c => c.isUpper)

  val pairs = List(1, 2, 3) zip s
  pairs.unzip

  s.flatMap(c => List('.', c))

  xs.sum
  xs.max

  val n = 7
  (1 until n).map(i => (1 until i).map(j => (i, j)))

  val romanNum = Map("I" -> 1, "V" -> 5, "X" -> 10)
  val capCountry = Map("Romania" -> "Bucharest")
  romanNum get "C"
  romanNum get "X"

  val fruits = List("apple", "pinapple", "orange", "banana", "pear")
  fruits.sortWith(_.length < _.length)
  fruits.sorted
  fruits.groupBy(_.head)
}