package FunctionalProgrammingPrinciplesScala.week6

/**
  * Created by flavia.nicoleta on 11/04/2018.
  */
object Combinations extends App {

  //list all combinations of x and y where x = 1..M and y = 1..N
  val M = 10
  val N = 15

  (1 to M).flatMap(x => (1 to N).map(y => (x, y)))

  //scalar product
  def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double = {
    (xs zip ys).map(xy => xy._1 * xy._2).sum
  }

  def scalarProduct1(xs: Vector[Double], ys: Vector[Double]): Double = {
    (xs zip ys).map { case (x, y) => x * y }.sum
  }

  def scalarProduct2(xs: Vector[Double], ys: Vector[Double]): Double = {
    (for ((x, y) <- xs zip ys) yield x * y).sum
  }

  def isPrime(n: Int): Boolean = (2 until n).forall(d => n % d != 0)

  def generatePrimeSeq(n: Int): Seq[(Int, Int)] = {
    (1 until n).flatMap(i => (1 until i).map(j => (i, j))).filter(pair => isPrime(pair._1 + pair._2))
  }

  def generatePrimeSeq1(n: Int): Seq[(Int, Int)] = {
    for {
      i <- 1 until n
      j <- i until n
      if isPrime(i + j)
    } yield (i, j)
  }

  println(generatePrimeSeq(7))

  case class Person(name: String, age: Int)

  def search(persons: List[Person]) = {
    for (p <- persons if p.age > 20) yield p.name
    // ===
    persons.filter(p => p.age > 20).map(p => p.name)
  }
}
