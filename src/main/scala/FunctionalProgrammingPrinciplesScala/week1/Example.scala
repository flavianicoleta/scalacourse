package FunctionalProgrammingPrinciplesScala.week1

/**
  * Created by flavia.nicoleta on 19/03/2018.
  */
object Example extends App {
  println("Hello World")

  def and(x: Boolean, y:Boolean) = if (x) y else false

  def or(x: Boolean, y:Boolean) = if (!x) y else true

}
