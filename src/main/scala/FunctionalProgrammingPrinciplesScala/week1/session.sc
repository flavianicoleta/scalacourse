object session {

  def sqrt(x: Double) = {
    def abs(x: Double) = if (x < 0) -x else x

    def sqrtIter(guess: Double): Double = {
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))
    }

    def isGoodEnough(guess: Double) = {
      abs(guess * guess - x) / x < 0.001
    }

    def improve(guess: Double) = {
      (guess + x / guess) / 2
    }

    sqrtIter(1.0)
  }

  def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)

  def factorial(n: Int) : Int = {

    def loop(acc: Int, n: Int) : Int = {
      if (n == 0) acc
      else loop(acc * n, n-1)
    }

    loop(1, n)
  }



  sqrt(2)
  sqrt(4)
//  sqrt(1e-6)
//  sqrt(1e60)

  gcd(14, 21)
  factorial(4)

}