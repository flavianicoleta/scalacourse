package FunctionalProgrammingPrinciplesScala.week4

/**
  * Created by flavia.nicoleta on 04/04/2018.
  */
trait Expr3 {
  def eval: Int = this match {
    case Number3(n) => n
    case Sum3(e1, e2) => e1.eval + e2.eval
  }

  def show: String = this match {
    case Number3(n) => n.toString
    case Sum3(e1, e2) => e1.show + " + " + e2.show
  }
}

case class Number3(n: Int) extends Expr3

case class Sum3(e1: Expr3, e2: Expr3) extends Expr3

object exprs extends App {
//  def show(e: Expr3) = e match {
//    case Number3(x) => x.toString
//    case Sum3(e1, e2) => show(e1) + " + " + show(e2)
//  }

  println(Sum3(Number3(1), Number3(44)).show)
}