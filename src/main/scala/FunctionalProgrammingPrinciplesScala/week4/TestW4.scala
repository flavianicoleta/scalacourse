package FunctionalProgrammingPrinciplesScala.week4

import FunctionalProgrammingPrinciplesScala.week3.{Empty, IntSet, NonEmpty}

/**
  * Created by flavia.nicoleta on 02/04/2018.
  */
object TestW4 extends App {

  val a: Array[NonEmpty] = Array(new NonEmpty(1, Empty, Empty))
//  val b: Array[IntSet] = a
//  b(0) = Empty
//  val s: NonEmpty = a(0)

}
