package FunctionalProgrammingPrinciplesScala.week4

/**
  * Created by flavia.nicoleta on 04/04/2018.
  */
object ListsExample extends App {
  val l1 = scala.List("a", "b", "c")
  val l2 : scala.List[Int] = scala.List(1, 2, 3)

//   l1 = "apples" :: "oranges" :: "pears" :: Nil

  def insert(x: Int, xs: scala.List[Int]): scala.List[Int] = xs match {
    case scala.List() => scala.List(x)
    case y :: ys => if (x <= y) x :: xs else y :: insert(x, ys)
  }

  def isort(xs: scala.List[Int]) : scala.List[Int] = xs match {
    case scala.List() => scala.List()
    case y :: ys => insert(y, isort(ys))
  }
}
