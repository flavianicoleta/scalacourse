package FunctionalProgrammingPrinciplesScala.week4

import FunctionalProgrammingPrinciplesScala.week3.Empty
import FunctionalProgrammingPrinciplesScala.week3.NonEmpty


/**
  * Created by flavia.nicoleta on 02/04/2018.
  */
trait List[+T] {
  def isEmpty: Boolean
  def head: T
  def tail: List[T]
  def prepend[U >: T](elem: U): List[U] = new Cons(elem, this)
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  override def isEmpty: Boolean = false
}

object Nil extends List[Nothing] {
  override def isEmpty: Boolean = true

  override def head: Nothing = throw new NoSuchElementException("Nil.head")

  override def tail: Nothing = throw new NoSuchElementException("Nil.tail")
}

object List {
//  def apply[T]() = new Nil
//
//  def apply[T](x1: T) : List[T] = new Cons(x1, new Nil)
//
//  def apply[T](x1: T, x2: T) : List[T] = new Cons(x1, new Cons(x2, new Nil))



}

object test {
  val x: List[String] = Nil

}