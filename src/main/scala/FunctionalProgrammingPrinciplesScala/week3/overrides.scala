package FunctionalProgrammingPrinciplesScala.week3

/**
  * Created by flavia.nicoleta on 27/03/2018.
  */
object overrides {
  def main(args: Array[String]): Unit = {
    println("hello world")
  }
}

abstract class Base {
  def foo = 1
  def bar: Int
}

class Sub extends Base {
  override def foo: Int = 2
  def bar = 3
}
