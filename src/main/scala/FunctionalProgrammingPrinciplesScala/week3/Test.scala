package FunctionalProgrammingPrinciplesScala.week3

import FunctionalProgrammingPrinciplesScala.week2.{Rational, FixedPoints}


/**
  * Created by flavia.nicoleta on 27/03/2018.
  */
object Test extends App {
  val t1 = new NonEmpty(3, Empty, Empty)
  val t2 = t1 incl 4

//  println(t1)
//  println(t2)

  new Rational(1, 3)

  def error(msg: String) = throw new Error(msg)

//  println(error("sss"))

  val x = null
//  println(x)

  val y: String = x
//  println(y)

//  val z: Int = null
//  println(z)

  val z = if(true) 1 else false
//  println(z)

  def singleton[T](elem: T) = new Cons[T](elem, new Nil[T])

//  println(singleton[Int](1))
//  println(singleton[Boolean](true))

  def nth(n: Int, list: List[Int]): Int = {
    if(n < 0) throw new IndexOutOfBoundsException("outside the range")

    def count(list: List[Int], nr: Int): Int = {
      if(nr <= n && list.isEmpty) throw new IndexOutOfBoundsException("outside the range")
      else if (nr == n && !list.isEmpty) list.head
      else count(list.tail, nr + 1)
    }

    count(list, 0)

  }

  def nth2[T](n: Int, list: List[T]): T = {
    if(list.isEmpty) throw new IndexOutOfBoundsException("outside the range")
    else if (n == 0) list.head
    else nth2(n - 1, list.tail)
  }

  val ll = new Cons[Int](1, new Cons[Int](2, new Cons[Int](3, new Cons[Int](4, new Cons[Int](5, new Nil[Int])))))
  println(nth2(10, ll))

}
