package FunctionalProgrammingPrinciplesScala.week3

/**
  * Created by flavia.nicoleta on 28/03/2018.
  */
trait Planar {

  def height: Int
  def width: Int
  def surface = height * width

}
