package FunctionalProgrammingPrinciplesScala.week5

/**
  * Created by flavia.nicoleta on 10/04/2018.
  */
class ListsFunctions {
  def last[T](xs: List[T]): T = xs match {
    case List() => throw new Error("last of empty list")
    case List(x) => x
    case y :: ys => last(ys)
  }

  def init[T](xs: List[T]): List[T] = xs match {
    case List() => throw new Error("init of empty list")
    case List(x) => List()
    case y :: ys => y :: init(ys)
  }

  def concat[T](xs: List[T], ys: List[T]) : List[T] = xs match {
    case List() => ys
    case z :: zs => z :: concat(zs, ys)
  }

  def reverse[T](xs: List[T]): List[T] = xs match {
    case List() => xs
    case y :: ys => reverse(ys) ++ List(y)
  }

  def removeAt[T](n: Int, xs: List[T]) = {
    if (n < 0 || n > xs.length) xs
    else (xs.take(n)) ::: (xs.drop(n+1))
  }

//  def flatten(xs: List[Any]): List[Any] =
}
