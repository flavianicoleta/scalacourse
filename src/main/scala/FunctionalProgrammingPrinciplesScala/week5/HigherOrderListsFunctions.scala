package FunctionalProgrammingPrinciplesScala.week5

/**
  * Created by flavia.nicoleta on 10/04/2018.
  */
object HigherOrderListsFunctions extends App {

  def scaleList(xs: List[Double], factor: Double) : List[Double] = xs match {
    case Nil => xs
    case y :: ys => y * factor :: scaleList(xs, factor)
  }

  def scaleList2(xs: List[Double], factor: Double) = xs.map(x => x * factor)

  def squareList(xs: List[Int]) : List[Int] = xs match {
    case Nil => xs
    case y :: ys => y * y :: squareList(ys)
  }

  def squareList2(xs: List[Int]) : List[Int] = xs.map(x => x * x)

  def posElems(xs: List[Int]): List[Int] = xs match {
    case Nil => Nil
    case y :: ys => if (y > 0) y :: posElems(ys) else posElems(ys)
  }

  def posElems2(xs: List[Int]): List[Int] = xs.filter(x => x > 0)

  val data = List("a", "a", "a", "b", "b", "c", "a")

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 =>
      val (first, rest) = xs span (y => y == x)
      first :: pack(rest)
  }

  def sum(xs: List[Int]) : Int = xs match {
    case Nil => 0
    case y :: ys => y + sum(ys)
  }

  def sum1(xs: List[Int]) = (0 :: xs) reduceLeft((x, y) => x + y)
  def product(xs: List[Int]) = (1 :: xs) reduceLeft(_ * _)
  def product1(xs: List[Int]) = (xs foldLeft 1)(_ * _)
  def sum2(xs: List[Int]) = (xs foldLeft 0)(_ + _)


  def encode[T](xs :List[T]) : List[(T, Int)] = pack(xs).map(list => (list.head, list.length))

  println(pack(data))
  println(encode(data))
}
