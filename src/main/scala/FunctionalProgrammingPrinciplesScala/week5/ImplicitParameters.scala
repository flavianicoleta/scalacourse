package FunctionalProgrammingPrinciplesScala.week5

/**
  * Created by flavia.nicoleta on 10/04/2018.
  */
object ImplicitParameters extends App{

  def msort[T](xs: List[T])(lt: (T, T) => Boolean): List[T] = {
    val n = xs.length/2
    if (n == 0) xs
    else {
      def merge(xs: List[T], ys: List[T]) : List[T] = (xs, ys) match {
        case (Nil, ys) => ys
        case (xs, Nil) => xs
        case (x :: xs1, y :: ys1) => if (lt(x, y)) x :: merge(xs1, ys)
        else y :: merge(xs, ys1)
      }
      val (fst, snd) = xs.splitAt(n)
      merge(msort(fst)(lt), msort(snd)(lt))
    }
  }

  def msortOrd[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
    val n = xs.length/2
    if (n == 0) xs
    else {
      def merge(xs: List[T], ys: List[T]) : List[T] = (xs, ys) match {
        case (Nil, ys) => ys
        case (xs, Nil) => xs
        case (x :: xs1, y :: ys1) => if (ord.lt(x, y)) x :: merge(xs1, ys)
        else y :: merge(xs, ys1)
      }
      val (fst, snd) = xs.splitAt(n)
      merge(msortOrd(fst), msortOrd(snd))
    }
  }

  val nums = List(2, -4, 5, 7, 1)
  val fruits = List("apple", "pinapple", "orange", "banana")
  println(msort(nums)((x, y) => x < y))
  println(msort(fruits)((x, y) => x.compareTo(y) < 0))


  println(msortOrd(nums))
  println(msortOrd(fruits)(Ordering.String))

}
