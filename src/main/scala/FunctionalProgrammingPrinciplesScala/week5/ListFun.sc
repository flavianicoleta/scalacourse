object listfun {
  val nums = List(2, -4, 5, 7, 1)
  val fruits = List("apple", "pinapple", "orange", "banana")

  nums filter(x => x > 0)
  nums filterNot(x => x > 0)
  nums partition(x => x > 0)

  nums takeWhile(x => x > 0)
  nums dropWhile(x => x > 0)
  nums span(x => x > 0)

  def concat[T](xs: List[T], ys: List[T]) : List[T] =
    (xs foldRight  ys)(_ :: _)
}