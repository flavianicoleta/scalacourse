package FunctionalProgrammingPrinciplesScala.week2

/**
  * Created by flavia.nicoleta on 23/03/2018.
  */
object SessionExercises extends App {

  def sum1(f : Int => Int, a: Int, b: Int): Int = {
    if (a > b) 0
    else f(a) + sum1(f, a+1, b)
  }

  def cube(x: Int) : Int = x * x * x
  def sum1Cubes(a: Int, b: Int): Int = sum1(cube, a, b)

  def sum1Ints(a: Int, b: Int) = sum1(x => x, a, b)

//  println(sum1Cubes(1, 10))
//  println(sum1Ints(1, 10))

  def sum(f: Int => Int)(a:Int, b:Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a + 1, acc + f(a))
    }

    loop(a, 0)
  }


  def sum2(f: Int => Int): (Int, Int) => Int = {
    def sumF(a: Int, b: Int): Int =
      if (a > b) 0
      else f(a) + sumF(a+1, b)
    sumF
   }

  def sum3(f: Int => Int)(a: Int, b: Int) : Int =
    if(a > b) 0 else f(a) + sum(f)(a+1, b)

//  println(sum(x => x)(1, 10))
//  println(sum(x => x * x)(1, 10))
//  println(sum(cube)(1, 10))
//
//  println(sum2(cube)(1, 10))


  def product(f : Int => Int)(a: Int, b: Int) : Int = {
    if (a > b) 1 else f(a) * product(f)(a+1, b)
  }

//  println(product(x => x)(3, 5))

  def factorialProd(n: Int) = product(x => x)(1, n)

//  println(factorialProd(4))

  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b:Int) : Int = {
    if (a > b) zero
    else combine(f(a), mapReduce(f, combine, zero)(a+1, b))
  }

  def prodMapReduce(f : Int => Int)(a: Int, b: Int) : Int = mapReduce(f, (x, y) => x * y, 1)(a, b)
  def sumMapReduce(f : Int => Int)(a: Int, b: Int) : Int = mapReduce(f, (x, y) => x + y, 0)(a, b)

//  println(mapReduce(x => x, (x, y) => x * y, 1)(1, 4))
//
//  println(prodMapReduce(x => x * x)(1, 3))
//  println(sumMapReduce(x => x * x)(1, 3))

  val x = new Rational(1, 3)
  val y = new Rational(5, 7)
  val z = new Rational(3, 2)

  val res = x- y -z

  println(y + y)

  println(x < y)

  println(x max y)


  val integer = new Rational(4)

  println(integer)

  val largenr = new Rational(21312, 12323)

  println(largenr)

  val xx = x + y
}
