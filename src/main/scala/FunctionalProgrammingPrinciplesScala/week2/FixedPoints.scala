package FunctionalProgrammingPrinciplesScala.week2

import math.abs
/**
  * Created by flavia.nicoleta on 24/03/2018.
  */
object FixedPoints extends App {

  val tolerance = 0.0001;
  def isCloseEnough(x: Double, y: Double) =
    abs((x -y) / x) / x < tolerance

  def fixedPoint(f: Double => Double)(firstGuess: Double) = {
    def iterate(guess: Double): Double = {
      val next = f(guess)
      if(isCloseEnough(guess, next)) next
      else iterate(next)
    }

    iterate(firstGuess)
  }

  def sqrt(x: Double) = fixedPoint(y => (y + x / y) / 2)(1)

  println(fixedPoint(x => 1 + x/2)(1))
  println(sqrt(2))

  def averageDamp(f: Double => Double)(x: Double) = (x + f(x)) / 2
  println(averageDamp(x => x/2)(2))

  def sqrt2(x: Double) = fixedPoint(averageDamp(y => x/y))(1)

  println(sqrt(16))
  println(sqrt2(16))

}
