package FunctionalProgrammingPrinciplesScala.week2

/**
  * Created by flavia.nicoleta on 25/03/2018.
  */
class Rational(x: Int, y: Int) {

  require(y != 0, "denomitor must not be 0")

  def this(x: Int) = this(x, 1)

  private def gcd(a: Int, b:Int): Int = if (b == 0) a else gcd (b, a % b)

  def numer = x
  def denom = y

  def unary_-  : Rational = new Rational(-numer, denom)

  def +(that: Rational) = new Rational(numer * that.denom + that.numer * denom, denom * that.denom)

  def -(that: Rational) = this + -that

  def mul(that: Rational) = new Rational(numer * that.numer, denom * that.denom)

  def div(that: Rational) = new Rational(numer * that.denom, denom * that.numer)

  def < (that: Rational) = this.numer * that.denom < that.numer * this.denom

  def max(that: Rational) = if (this < that) that else this

  override def toString: String = {
    val g = gcd(numer, denom)
    numer / g  + "/" + denom / g
  }

}
