package FunctionalProgramDesignScala.week3.simulation

/**
  * Created by flavia.gheorghe on 10/05/2018.
  */
trait Parameters {
  def InverterDelay = 2
  def AddGateDelay = 3
  def OrGateDelay = 5
}
