package FunctionalProgramDesignScala.week3.simulation

/**
  * Created by flavia.gheorghe on 10/05/2018.
  */
abstract class Circuits extends Gates {
    def halfAdder(a: Wire, b: Wire, s: Wire, c: Wire): Unit = {
      val d, e = new Wire
      orGate(a, b, d)
      andGate(a, b, c)
      inverter(c, e)
      andGate(d, e, s)
    }

    def fullAdder(a: Wire, b: Wire, sum: Wire, cin: Wire, cout: Wire): Unit = {
      val s, c1, c2 = new Wire
      halfAdder(b, cin, s, c1)
      halfAdder(a, s, sum, c2)
      orGate(c1, c2, cout)
    }
}
