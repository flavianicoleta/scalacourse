package FunctionalProgramDesignScala.week3

import FunctionalProgramDesignScala.week4.Publisher
import FunctionalProgramDesignScala.week4.frf.Var

/**
  * Created by flavia.nicoleta on 07/05/2018.
  */
class BankAccount extends Publisher {
  private val balance = new Var(0)

  def currentBalance: Int = balance()

  def deposit(amount: Int): Unit ={
    val b = balance()
    if (amount > 0) balance() = b + amount
    publish()
  }

  def withdraw(amount: Int): Unit = {
    if (0 < amount && amount < balance()) {
      val b = balance()
      balance() = b - amount
      publish()
    } else throw new Error("Insufficient funds")

  }
}
