import FunctionalProgramDesignScala.week3.BankAccount

object account {
  val account = new BankAccount

  account deposit 50
  account withdraw 20
  account withdraw 20

//  account withdraw 15

  val x = new BankAccount
  val y = new BankAccount
//  x deposit 30
//  y withdraw 20

  x deposit 30
  x withdraw 10

  def power(x: Double, exp: Int): Double = {
    var r = 1.0
    var i = exp
    while (i > 0) {r = r*x; i = i-1}
    r
  }

  power(2, 4)

  for(i <- 1 until 3) {System.out.print(i + " ")}

  for(i <- 1 until 3; j <-"abc") println(i + " " + j)

}