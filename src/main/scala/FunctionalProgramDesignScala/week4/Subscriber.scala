package FunctionalProgramDesignScala.week4

import FunctionalProgramDesignScala.week3.BankAccount

/**
  * Created by flavia.nicoleta on 20/05/2018.
  */
trait Subscriber {
  def handler(publisher: Publisher)
}


