package FunctionalProgramDesignScala.week4.frf

/**
  * Created by flavia.nicoleta on 20/05/2018.
  */
class StackableVariable[T](init: T) {
  private var values: List[T] = List(init)

  def value: T = values.head
  def withValue[R](newValue: T)(op: => R): R = {
    values = newValue :: values
    try op finally values = values.tail
  }
}
