package FunctionalProgramDesignScala.week4.frf

/**
  * Created by flavia.nicoleta on 20/05/2018.
  */
class Var[T](expr: =>T) extends Signal[T](expr) {
  override def update(expr: => T): Unit = super.update(expr)
}

object Var {
  def apply[T](expr: => T)= new Var(expr)
}
