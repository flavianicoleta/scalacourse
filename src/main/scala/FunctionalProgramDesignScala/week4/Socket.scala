package FunctionalProgramDesignScala.week4

import java.net.Socket

import scala.concurrent.Future

/**
  * Created by flavia.nicoleta on 20/05/2018.
  */
trait Socket {
  def readFromMemory(): Future[Array[Byte]]
  def sendToEurope(coins: Array[Byte]): Future[Array[Byte]]

}

object test {}