package FunctionalProgramDesignScala.week4

import FunctionalProgramDesignScala.week3.BankAccount

/**
  * Created by flavia.nicoleta on 20/05/2018.
  */
class Consolidator(observed: List[BankAccount]) extends Subscriber {

  observed.foreach(_.subscribe(this))

  private var total: Int = _
  compute()

  private def compute() =
    total = observed.map(_.currentBalance).sum

  def handler(publisher: Publisher) = compute()

  def totalBalance = total
}
