package FunctionalProgramDesignScala.week1

/**
  * Created by flavia.nicoleta on 28/04/2018.
  */

trait Tree

case class Inner(left: Tree, right: Tree) extends Tree

case class Leaf(x: Int) extends Tree


trait Generator[+T] {
  self => //an alias for this

  def generate: T

  def map[S](f: T => S): Generator[S] = new Generator[S] {
    override def generate: S = f(self.generate)
  }

  def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
    override def generate: S = f(self.generate).generate
  }
}


object GeneratorTest extends App {
  val integers = new Generator[Int] {
    val rand = new java.util.Random

    override def generate: Int = rand.nextInt()
  }

  val booleans = new Generator[Boolean] {
    override def generate: Boolean = integers.generate > 0
  }

  def pairs[T, U](t: Generator[T], u: Generator[U]): Generator[(T, U)] = for {
    x <- t
    y <- u
  } yield (x, y)


  def single[T](x: T): Generator[T] = new Generator[T] {
    override def generate: T = x
  }

  def choose(lo: Int, hi: Int): Generator[Int] = for (x <- integers) yield lo + x % (hi - lo)

  def onfOf[T](xs: T*): Generator[T] = for (idx <- choose(0, xs.length)) yield xs(idx)



  def lists: Generator[List[Int]] = for {
    isEmpty <- booleans
    list <- if (isEmpty) emptyLists else nonEmptyLists
  } yield list

  def emptyLists = single(Nil)

  def nonEmptyLists = for {
    head <- integers
    tail <- lists
  } yield head :: tail


  def leafs: Generator[Leaf] = for {
    x <- integers
  } yield Leaf(x)

  def inners: Generator[Inner] = for {
    l <- trees
    r <- trees
  } yield Inner(l, r)

  def trees: Generator[Tree] = for {
    isLeaf <- booleans
    tree <- if (isLeaf) leafs else inners
  } yield tree

//  println(trees.generate)

  def test[T](g: Generator[T], numTimes: Int = 100)(test : T => Boolean): Unit = {
    for(i <- 0 until numTimes) {
      val value = g.generate
      assert(test(value), "test failed for " +  value)
    }
    println("passed " + numTimes + " tests")
  }

  test(pairs(lists, lists)) {
    case (xs, ys) => (xs ++ ys).length > xs.length
  }

}
