object test {

  val f : PartialFunction[String, String] = {case "ping" => "pong"}
  f("ping")
  f.isDefinedAt("ping")
  f.isDefinedAt("abs")
//  f("abs")

  val g: PartialFunction[List[Int], String] = {
    case Nil => "one"
    case x :: y :: rest => "two"
  }

  val h: PartialFunction[List[Int], String] = {
    case Nil => "one"
    case x :: rest =>
      rest match {
        case Nil => "two"
      }
  }

  g.isDefinedAt(List(1, 2, 3))
  h.isDefinedAt(List(1, 2, 3))

}