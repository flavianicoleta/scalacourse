package FunctionalProgramDesignScala.week1

import scala.util.control.NonFatal

/**
  * Created by flavia.nicoleta on 29/04/2018.
  */
abstract class Try[T] {
//  def flatMap[U](f: T => Try[U]): Try[U] = this match {
//    case Success(x) => try f(x) catch {case NonFatal(ex) => Failure(ex)}
//    case fail: Failure => fail
//  }
}
case class Success[T](x: T) extends Try[T]
case class Failure(ex: Exception) extends Try[Nothing]

//object Try {
//  def apply[T](expr: => T): Try[T] =
//    try Success(expr)
//    catch {
//      case NonFatal(ex) => Failure(ex)
//    }
//
//}