package FunctionalProgramDesignScala.week2

import FunctionalProgrammingPrinciplesScala.week6.Combinations

/**
  * Created by flavia.nicoleta on 29/04/2018.
  */
object StreamsEx extends App {
  val xs = Stream.cons(1, Stream.cons(2, Stream.Empty))

  val ys = Stream(1, 2, 3)

  (1 to 1000).toStream

  def streamRange(lo: Int, hi:Int): Stream[Int] = {
    print(lo + " ")
    if (lo >= hi) Stream.Empty
    else Stream.cons(lo, streamRange(lo + 1, hi))
  }

  println(xs)

  println(((1000 to 10000).toStream filter Combinations.isPrime) apply 1)

  streamRange(1, 10).take(3).toList

  def expr = {
    val x = { print("x"); 1}
    lazy val y = { print("y"); 2}
    def z = { print("z"); 3}

    z + y + x + z + y + x
  }

  println()
  println(expr) //xzyz

  def from(n: Int): Stream[Int] = n #:: from(n+1)
  val nats = from(0)

  val m4s = nats map (_ * 4)

  println(m4s.take(100).toList)

  def sieve(s: Stream[Int]): Stream[Int] = s.head #:: sieve(s.tail filter (_ % s.head != 0))

  val primes = sieve(from(2))

  println(primes.take(100).toList)

  def sqrtStram(x: Double): Stream[Double] = {
    def improve(guess: Double) = (guess + x / guess) / 2
    lazy val guesses: Stream[Double] = 1 #:: (guesses map improve)
    guesses
  }

  def isGoogEnough(guess: Double, x: Double) = math.abs((guess * guess - x) / x) < 0.0001

  println(sqrtStram(4).take(10).toList)
  println(sqrtStram(4).filter(isGoogEnough(_, 4)).take(10).toList)

}
